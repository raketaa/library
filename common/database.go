package common

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx"
)

var DB *pgx.Conn

func InitDB() *pgx.Conn {
	conn, err := pgx.Connect(context.Background(), os.Getenv("POSTGRESS_CONN"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connection to database: %v\n", err)
		os.Exit(1)
	}

	DB = conn
	return DB
}

func GetDB() *pgx.Conn {
	return DB
}
