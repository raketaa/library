package rentals

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.RouterGroup) {
	router.POST("/rent", Rent)
	router.POST("/return", Return)
	router.GET("/", View)
}

func Rent(c *gin.Context) {
	u := c.PostForm("user")
	b := c.PostForm("book")

	ui, err := strconv.Atoi(u)
	bi, err := strconv.Atoi(b)

	err = RentBook(ui, bi)
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.String(http.StatusOK, "Success")
}

func Return(c *gin.Context) {
	u := c.PostForm("user")
	b := c.PostForm("book")

	ui, err := strconv.Atoi(u)
	bi, err := strconv.Atoi(b)

	err = ReturnBook(ui, bi)
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.String(http.StatusOK, "Success")
}

func View(c *gin.Context) {

	r, err := ListRentals()
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, r)
}
