package rentals

import (
	"context"
	"errors"
	"library/common"
	"time"
)

type Rental struct {
	UserId    int       `json:"user_id"`
	BookId    int       `json:"book_id"`
	Rented    bool      `json:"rented"`
	CreatedAt time.Time `json:"created_at"`
}

func RentBook(userId int, bookId int) error {
	db := common.GetDB()

	tx, err := db.Begin(context.Background())
	if err != nil {
		return err
	}
	defer tx.Rollback(context.Background())

	var s int
	err = tx.QueryRow(context.Background(), "SELECT stock FROM books WHERE id=$1 FOR UPDATE NOWAIT", bookId).Scan(&s)
	if err != nil {
		return err
	}
	if s == 0 {
		return errors.New("Book doesn't have sufficient stock, something went wrong :)")
	}

	_, err = tx.Exec(context.Background(), "INSERT INTO rentals(user_id, book_id, rented, created_at) VALUES ($1, $2, true, NOW())", userId, bookId)
	if err != nil {
		return err
	}

	_, err = tx.Exec(context.Background(), "UPDATE books SET stock = stock - 1 WHERE id = $1", bookId)
	if err != nil {
		return err
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func ReturnBook(userId int, bookId int) error {
	db := common.GetDB()
	tx, err := db.Begin(context.Background())
	if err != nil {
		return err
	}
	defer tx.Rollback(context.Background())

	var a int
	var s int
	err = tx.QueryRow(context.Background(), "SELECT quantity, stock FROM books WHERE id=$1 FOR UPDATE NOWAIT", bookId).Scan(&a, &s)
	if err != nil {
		return err
	}
	if s == a {
		return errors.New("Stock is full, something went wrong :)")
	}

	_, err = tx.Exec(context.Background(), "INSERT INTO rentals(user_id, book_id, rented, created_at) values ($1, $2, false, NOW())", userId, bookId)
	if err != nil {
		return err
	}

	_, err = tx.Exec(context.Background(), "UPDATE books SET stock = stock + 1 WHERE id = $1", bookId)
	if err != nil {
		return err
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func ListRentals() ([]Rental, error) {
	db := common.GetDB()

	rows, err := db.Query(context.Background(), "SELECT user_id, book_id, rented, created_at FROM rentals ORDER BY created_at DESC LIMIT 30")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rentals []Rental
	for rows.Next() {
		var r Rental

		err = rows.Scan(&r.UserId, &r.BookId, &r.Rented, &r.CreatedAt)
		if err != nil {
			return nil, err
		}

		rentals = append(rentals, r)
	}

	if rows.Err() != nil {
		return nil, err
	}

	return rentals, nil
}
