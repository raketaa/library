package main

import (
	"context"
	"library/books"
	"library/common"
	"library/rentals"
	"library/users"

	"github.com/gin-gonic/gin"
)

func main() {
	db := common.InitDB()
	defer db.Close(context.Background())

	r := gin.Default()

	api := r.Group("/api")
	users.Routes(api.Group("/users"))
	books.Routes(api.Group("/books"))
	rentals.Routes(api.Group("/rentals"))

	r.Run()
}
