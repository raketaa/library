package users

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.RouterGroup) {
	router.GET("/", UserAll)
	router.GET("/:id", UserGet)
	router.POST("/create", UserCreation)
}

func UserAll(c *gin.Context) {
	u, err := ListUsers()
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, u)
}

func UserGet(c *gin.Context) {
	ids := c.Param("id")
	id, err := strconv.Atoi(ids)
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	u, err := GetUser(id)
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, u)
}

func UserCreation(c *gin.Context) {
	fn := c.PostForm("firstname")
	ln := c.PostForm("lastname")

	err := CreateUser(fn, ln)
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
	}

	c.String(http.StatusOK, "Success")
}
