package users

import (
	"context"
	"library/common"
)

type User struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

func ListUsers() ([]User, error) {
	db := common.GetDB()

	rows, err := db.Query(context.Background(), "SELECT firstname, lastname FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var u User

		err = rows.Scan(&u.Firstname, &u.Lastname)
		if err != nil {
			return nil, err
		}

		users = append(users, u)
	}

	if rows.Err() != nil {
		return nil, err
	}

	return users, nil
}

func GetUser(id int) (User, error) {
	db := common.GetDB()

	var u User
	err := db.QueryRow(context.Background(), "SELECT firstname, lastname FROM users WHERE id=$1", id).Scan(&u.Firstname, &u.Lastname)
	if err != nil {
		return u, err
	}

	return u, nil
}

func CreateUser(fn string, ln string) error {
	db := common.GetDB()

	_, err := db.Exec(context.Background(), "INSERT INTO users (firstname, lastname) VALUES ($1,$2)", fn, ln)
	if err != nil {
		return err
	}

	return nil
}
