--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: books; Type: TABLE; Schema: public; Owner: library
--

CREATE TABLE public.books (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    quantity integer NOT NULL,
    stock integer NOT NULL
);


ALTER TABLE public.books OWNER TO library;

--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: library
--

CREATE SEQUENCE public.books_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.books_id_seq OWNER TO library;

--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: library
--

ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;


--
-- Name: rentals; Type: TABLE; Schema: public; Owner: library
--

CREATE TABLE public.rentals (
    id integer NOT NULL,
    user_id integer NOT NULL,
    book_id integer NOT NULL,
    rented boolean NOT NULL,
    created_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.rentals OWNER TO library;

--
-- Name: rentals_id_seq; Type: SEQUENCE; Schema: public; Owner: library
--

CREATE SEQUENCE public.rentals_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rentals_id_seq OWNER TO library;

--
-- Name: rentals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: library
--

ALTER SEQUENCE public.rentals_id_seq OWNED BY public.rentals.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: library
--

CREATE TABLE public.users (
    id integer NOT NULL,
    firstname character varying(255) NOT NULL,
    lastname character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO library;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: library
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO library;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: library
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: books id; Type: DEFAULT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.books ALTER COLUMN id SET DEFAULT nextval('public.books_id_seq'::regclass);


--
-- Name: rentals id; Type: DEFAULT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.rentals ALTER COLUMN id SET DEFAULT nextval('public.rentals_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: books books_pkey; Type: CONSTRAINT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: rentals rentals_pkey; Type: CONSTRAINT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.rentals
    ADD CONSTRAINT rentals_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: rentals rentals_book_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.rentals
    ADD CONSTRAINT rentals_book_id_foreign FOREIGN KEY (book_id) REFERENCES public.books(id);


--
-- Name: rentals rentals_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: library
--

ALTER TABLE ONLY public.rentals
    ADD CONSTRAINT rentals_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--
