package books

import (
	"context"
	"library/common"
)

type Book struct {
	Title string `json:"title"`
	Stock int    `json:"stock"`
}

func ListBooks() ([]Book, error) {
	db := common.GetDB()

	rows, err := db.Query(context.Background(), "SELECT title, stock FROM books")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []Book
	for rows.Next() {
		var b Book

		err = rows.Scan(&b.Title, &b.Stock)
		if err != nil {
			return nil, err
		}

		books = append(books, b)
	}

	if rows.Err() != nil {
		return nil, err
	}

	return books, nil
}
