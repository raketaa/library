package books

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Routes(router *gin.RouterGroup) {
	router.GET("/", BookAll)
}

func BookAll(c *gin.Context) {
	b, err := ListBooks()
	if err != nil {
		c.JSON(http.StatusExpectationFailed, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, b)
}
